const express = require('express');
const app = express();
const port = 8000;
const Caesar = require('caesar-salad').Caesar;

app.get('/encode/:name', (req, res) => {
    res.send(Caesar.Cipher('whatever').crypt(req.params.name));
});

app.get('/decode/:name', (req, res) => {
    res.send(Caesar.Decipher('whatever').crypt(req.params.name));
});

app.get('/:name', (req, res) => {
    res.send(req.params.name);
});
app.listen(port, () => {
    console.log('We are live on ' + port);
})